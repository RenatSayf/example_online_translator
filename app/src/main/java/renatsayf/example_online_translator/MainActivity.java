package renatsayf.example_online_translator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TranslatorApi.icallBack
{
    private EditText editTextEnter;
    private TextView textViewResult;
    private TextView textViewResult2;
    private ProgressBar progressBar;
    private Translator translator;
    private TranslatorApi translatorApi;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        textViewResult_onChange();

        //translatorApi = new TranslatorApi(this);
    }

    private void initViews()
    {
        editTextEnter = (EditText) findViewById(R.id.textViewEnter);
        textViewResult= (TextView) findViewById(R.id.textViewResult);
        textViewResult2 = (TextView) findViewById(R.id.textViewResult2);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        translator = new Translator(textViewResult2, progressBar);

    }
    private void textViewResult_onChange()
    {
        textViewResult.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                //Toast.makeText(MainActivity.this,"Перевод - "+translator.getResultAsyncTask(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
    }

    public static String trans_text;

    public void buttonTrans_Click(View view)
    {
        trans_text = null;
        progressBar.setVisibility(View.VISIBLE);
        translatorApi = new TranslatorApi(this);
        if (!editTextEnter.getText().equals(null))
        {
            try
            {
                translator.getTranslateAsync(editTextEnter.getText().toString());
                translatorApi.execute(editTextEnter.getText().toString());
            }
            catch (Exception e)
            {
                e.printStackTrace();
                z_Log.v("Исключение ExecutionException - " + e.getMessage());
            }
        }

    }

    @Override
    public void asyncResult(ArrayList<String> result)
    {
        textViewResult.setText(result.get(0).toString());
        progressBar.setVisibility(View.GONE);
    }

}
