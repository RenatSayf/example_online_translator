package renatsayf.example_online_translator;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Ренат on 06.06.2016.
 */
public class Translator
{
    private HttpURLConnection connection;
    private String undefined = "{\"head\":{},\"def\":[{\"text\":\"unterminated\",\"pos\":\"прил\",\"tr\":[{\"text\":\"неопределено\",\"pos\":\"прил\"}]}]}";
    private TextView textView;
    private ProgressBar progressBar;

    public Translator(TextView textView, ProgressBar progressBar)
    {
        this.textView = textView;
        this.progressBar = progressBar;

    }


    public void getTranslateAsync(final String text)
    {
        AsyncTask asyncTask = new AsyncTask()
        {
            @Override
            protected void onPreExecute()
            {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected Object doInBackground(Object[] params)
            {
                String _content = null;
                try
                {
                    _content = getContentFromTranslator(text);
                } catch (Exception e)
                {
                    e.printStackTrace();
                    z_Log.v("Исключение - " + e.getMessage());
                }
                return _content;
            }

            @Override
            protected void onPostExecute(Object o)
            {
                progressBar.setVisibility(View.GONE);
                ArrayList<String> list = getWords(o.toString());
                String str = "";
                for (int i = 0; i < list.size(); i++)
                {
                    if(i > 2) break;
                    str = str + list.get(i) + ", ";
                    z_Log.v("list[] = "+list.get(i));
                }
                textView.setText(str);
                z_Log.v("str = "+str);
            }
        };
        asyncTask.execute();
    }

    private String getContentFromTranslator(String text)
    {
        String lang = getLangTranslate(text)[0];
        String ui = getLangTranslate(text)[1];
        if (lang == null || ui == null)
        {
            return undefined;
        }
        z_Log.v("lang = " + lang + "    ui = " + ui);

        String text_encode = null;
        try
        {
            text_encode = URLEncoder.encode(text,"utf-8");
        } catch (UnsupportedEncodingException e)
        {
            z_Log.v("Исключение = " + e.getMessage());
        }

        String link = "https://dictionary.yandex.net/dicservice.json/lookup?ui="+ui+"&srv=tr-touch&sid=80448f92.5756df9e.029508ee&text="+text_encode+"&type=&lang="+lang+"&flags=7";

        BufferedReader reader = null;
        try
        {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept","*/*");
            connection.setRequestProperty("Accept-Encoding","utf-8");
            connection.setRequestProperty("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            connection.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36");

            connection.setDoOutput(true);
            connection.setReadTimeout(100000);
            connection.connect();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder buf=new StringBuilder();
            String line=null;
            while ((line=reader.readLine()) != null)
            {
                buf.append(line + "\n");
            }
            return(buf.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            z_Log.v("Исключение - " + e.getMessage());
            return undefined;
        }
        finally
        {
            if (reader != null)
            {
                try
                {
                    reader.close();
                } catch (Exception e)
                {
                    z_Log.v("Иключение - " + e.getMessage());
                }
            }
        }
    }
    private String[] getLangTranslate(String text)
    {
        String str = text;
        String[] lang = new String[2];
        for (int i = 0; i < str.length(); i++)
        {
            int char_first = str.codePointAt(0);
            if ((char_first >= 33 && char_first <= 64) || (str.codePointAt(i) >= 91 && str.codePointAt(i) <= 96) || (str.codePointAt(i) >= 123 && str.codePointAt(i) <= 126))
            {
                continue;
            }
            if (str.codePointAt(i) >= 1025 && str.codePointAt(i) <= 1105)
            {
                lang[0] = "ru-en";
                lang[1] = "ru";
            }
            else if (str.codePointAt(i) >= 65 && str.codePointAt(i) <= 122)
            {
                lang[0] = "en-ru";
                lang[1] = "en";
            }
            else
            {
                lang[0] = null;
                lang[1] = null;
            }
        }
        return lang;
    }
    public ArrayList<String> getWords(String json_str) //throws JSONException
    {
        JSONObject jsonObject;
        JSONArray def;
        ArrayList<String> list=new ArrayList<>();
        try
        {
            jsonObject = new JSONObject(json_str);
            def = jsonObject.getJSONArray("def");
            for (int i = 0; i < def.length(); i++)
            {
                JSONObject object = def.getJSONObject(i);
                JSONArray tr = object.getJSONArray("tr");
                for (int j = 0; j < tr.length(); j++)
                {
                    JSONObject object1 = tr.getJSONObject(j);
                    String str = object1.getString("text");
                    list.add(str);
                }
            }
        } catch (JSONException e)
        {
            z_Log.v("Исключение - "+e.getMessage());
        }
        return list;
    }


}
